package main

import (
	"github.com/go-testfixtures/testfixtures/v3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/eleven/config"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/eleven/repository/mysqlrepo"
)

func main() {
	// read configuration
	conf, err := config.ReadConfig()
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	if conf.Service.Prod == false {
		log.SetLevel(log.TraceLevel)
	}

	// connect to database
	db, err := mysqlrepo.NewMysqlClient(conf)
	if err != nil {
		log.Panicf("failed to connect to mysql: %v", err)
	}
	defer db.Close()

	db.DropTableIfExists(
		&model.Payment{},
		&model.CashOut{},
		&model.CashOut{},
	)
	db.AutoMigrate(
		&model.Payment{},
		&model.CashOut{},
		&model.CashOut{},
	)

	fixtures, err := testfixtures.New(
		testfixtures.Database(db.DB()),
		testfixtures.Dialect(mysqlrepo.DatabaseDialect),
		testfixtures.DangerousSkipTestDatabaseCheck(),
		testfixtures.Directory("fixtures"),
	)
	if err != nil {
		log.Panicf("failed to create fixtures: %v", err)
	}

	if err := fixtures.Load(); err != nil {
		log.Panicf("failed to load fixtures: %v", err)
	}
}

package workflow

import (
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/eleven/repository"
)

type PaymentUpdater struct {
	goflow.Component

	In         <-chan *model.Payment
	Out        chan<- *model.Payment
	Error      chan<- error
	Repository repository.PaymentRepositoryInterface
}

func (pu *PaymentUpdater) Process() {
	for p := range pu.In {

		if err := pu.Repository.Update(p); err != nil {
			log.WithError(err).Error()
			pu.Error <- err
			return
		}

		pu.Out <- p
	}
}

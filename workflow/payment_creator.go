package workflow

import (
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/eleven/repository"
)

type PaymentCreator struct {
	goflow.Component

	In         <-chan *model.Payment
	Out        chan<- *model.Payment
	Error      chan<- error
	Repository repository.PaymentRepositoryInterface
}

func (tsk *PaymentCreator) Process() {
	for p := range tsk.In {

		if err := tsk.Repository.Insert(p); err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			return
		}

		tsk.Out <- p
	}
}

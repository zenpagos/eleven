package workflow

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/tools"
	pbv1will "gitlab.com/zenpagos/will/proto/v1"
)

type AccountParamGetter struct {
	goflow.Component

	In         <-chan *model.Payment
	Out        chan<- *model.Payment
	Error      chan<- error
	WillClient pbv1will.WillServiceClient
}

func (tsk *AccountParamGetter) Process() {
	for p := range tsk.In {

		req := pbv1will.GetInformationToExecuteAPaymentRequest{
			Account:           p.AccountID.String(),
			PaymentMethodType: p.PaymentMethodType,
			PaymentMethod:     p.PaymentMethod,
			Installments:      p.Installments,
			Amount:            p.Amount,
		}

		res, err := tsk.WillClient.GetInformationToExecuteAPayment(context.Background(), &req)
		if err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			return
		}

		// decidir information
		p.DecidirPrivateKey = tools.StringPointerOrNil(res.DecidirPrivateKey)

		// fee information
		p.CardCoefficient = res.CardCoefficient
		p.ProcessingCostPercentage = res.ProcessingCostPercentage
		p.ProcessingCostFixedAmount = res.ProcessingCostFixedAmount
		p.FeePercentage = res.FeePercentage
		p.FeeFixedAmount = res.FeeFixedAmount
		p.DaysOnHold = res.DaysOnHold
		p.InstallmentAmount = res.InstallmentAmount
		p.PaidAmount = res.AmountToPay
		p.TaxesAmount = res.TaxesAmount
		p.FeeAmount = res.FeeAmount
		p.ReceivedAmount = res.ReceivedAmount
		p.AmountReleaseDate = res.AmountReleaseDate
		p.StatementDescriptor = tools.StringPointerOrNil(res.StatementDescriptor)

		// merchant information
		p.MerchantTIN = res.MerchantTIN
		p.MerchantTINType = res.MerchantTINType
		p.MerchantName = res.MerchantName
		p.MerchantStreet = res.MerchantStreet
		p.MerchantDoorNumber = res.MerchantDoorNumber
		p.MerchantZipCode = res.MerchantZipCode
		p.MerchantCategory = res.MerchantCategory
		p.MerchantGeographicCode = res.MerchantGeographicCode
		p.MerchantCity = res.MerchantCity
		p.MerchantID = res.MerchantID
		p.MerchantProvince = res.MerchantProvince
		p.MerchantCountry = res.MerchantCountry
		p.MerchantEmail = res.MerchantEmail
		p.MerchantPhone = res.MerchantPhone

		tsk.Out <- p
	}
}

package workflow

import (
	"github.com/trustmaster/goflow"
	pbv1dustin "gitlab.com/zenpagos/dustin/proto/v1"
	"gitlab.com/zenpagos/eleven/repository"
	"gitlab.com/zenpagos/eleven/wrapper/wkafka"
	pbv1lucas "gitlab.com/zenpagos/lucas/proto/v1"
	pbv1maxine "gitlab.com/zenpagos/maxine/proto/v1"
	pbv1will "gitlab.com/zenpagos/will/proto/v1"
)

const (
	// Payment
	checkoutGetter     = "checkout.getter"
	accountParamGetter = "account.param.getter"
	paymentCreator     = "payment.creator"
	paymentProcessor   = "payment.processor"
	paymentCancelator  = "payment.cancelator"
	paymentUpdater     = "payment.updater"
	checkoutUpdater    = "checkout.updater"
	balanceUpdater     = "balance.updater"
	sendEmailToPayer   = "send.email.to.payer"

	// Cash out
	canCashOut                = "can.cash.out"
	calculateFeeCashOut       = "calculate.fee.cash.out"
	persistCashOut            = "persist.cash.out"
	updateBalanceAfterCashOut = "update.balance.after.cash.out"

	errorReporter = "error.reporter"
)

func NewPaymentWithCardWorkflow(
	pr repository.PaymentRepositoryInterface,
	kw *wkafka.KafkaWrapper,
	dcl pbv1dustin.DustinServiceClient,
	lcl pbv1lucas.LucasServiceClient,
	mcl pbv1maxine.MaxineServiceClient,
	wcl pbv1will.WillServiceClient,
) *goflow.Graph {

	wf := goflow.NewGraph()

	// create components
	cg := new(CheckoutGetter)
	cg.DustinClient = dcl

	apg := new(AccountParamGetter)
	apg.WillClient = wcl

	pc := new(PaymentCreator)
	pc.Repository = pr

	pp := new(PaymentProcessor)
	pp.LucasClient = lcl

	pca := new(PaymentCancelator)
	pca.Repository = pr

	pu := new(PaymentUpdater)
	pu.Repository = pr

	cu := new(CheckoutUpdater)
	cu.DustinClient = dcl

	bu := new(BalanceUpdater)
	bu.MaxineClient = mcl

	se := new(SendEmailToPayer)
	se.WKafka = kw

	er := new(ErrorReporter)

	// add processes to the network
	wf.Add(checkoutGetter, cg)
	wf.Add(accountParamGetter, apg)
	wf.Add(paymentCreator, pc)
	wf.Add(paymentProcessor, pp)
	wf.Add(paymentCancelator, pca)
	wf.Add(paymentUpdater, pu)
	wf.Add(checkoutUpdater, cu)
	wf.Add(balanceUpdater, bu)
	wf.Add(sendEmailToPayer, se)
	wf.Add(errorReporter, er)

	// connect channels
	wf.Connect(checkoutGetter, "Out", accountParamGetter, "In")
	wf.Connect(checkoutGetter, "Error", errorReporter, "In")

	wf.Connect(accountParamGetter, "Out", paymentCreator, "In")
	wf.Connect(accountParamGetter, "Error", errorReporter, "In")

	wf.Connect(paymentCreator, "Out", paymentProcessor, "In")
	wf.Connect(paymentCreator, "Error", errorReporter, "In")

	// if there is any error processing, the payment is updated to a rejected status
	wf.Connect(paymentProcessor, "Out", paymentUpdater, "In")
	wf.Connect(paymentProcessor, "Error", errorReporter, "In")
	wf.Connect(paymentProcessor, "PaymentProcessFail", paymentCancelator, "In")
	wf.Connect(paymentCancelator, "Error", errorReporter, "In")

	wf.Connect(paymentUpdater, "Out", checkoutUpdater, "In")
	wf.Connect(paymentUpdater, "Error", errorReporter, "In")

	wf.Connect(checkoutUpdater, "Out", balanceUpdater, "In")
	wf.Connect(checkoutUpdater, "Error", errorReporter, "In")

	wf.Connect(balanceUpdater, "Out", sendEmailToPayer, "In")
	wf.Connect(balanceUpdater, "Error", errorReporter, "In")

	// final node
	wf.Connect(sendEmailToPayer, "Error", errorReporter, "In")

	// map ports
	wf.MapInPort("In", checkoutGetter, "In")
	wf.MapOutPort("Out", sendEmailToPayer, "Out")
	wf.MapOutPort("Error", errorReporter, "Out")

	return wf
}

func NewCashOutWorkflow(
	cor repository.CashOutRepositoryInterface,
	mcl pbv1maxine.MaxineServiceClient,
	wcl pbv1will.WillServiceClient,
) *goflow.Graph {

	wf := goflow.NewGraph()

	cco := new(CanCashOut)
	cco.MaxineClient = mcl

	cfco := new(CalculateFeeCashOut)
	cfco.WillClient = wcl

	coc := new(PersistCashout)
	coc.Repository = cor

	ub := new(UpdateBalanceAfterCashOut)
	ub.MaxineClient = mcl

	er := new(ErrorReporter)

	wf.Add(canCashOut, cco)
	wf.Add(calculateFeeCashOut, cfco)
	wf.Add(persistCashOut, coc)
	wf.Add(updateBalanceAfterCashOut, ub)
	wf.Add(errorReporter, er)

	wf.Connect(canCashOut, "Out", calculateFeeCashOut, "In")
	wf.Connect(canCashOut, "Error", errorReporter, "In")

	wf.Connect(calculateFeeCashOut, "Out", persistCashOut, "In")
	wf.Connect(calculateFeeCashOut, "Error", errorReporter, "In")

	wf.Connect(persistCashOut, "Out", updateBalanceAfterCashOut, "In")
	wf.Connect(persistCashOut, "Error", errorReporter, "In")

	wf.Connect(updateBalanceAfterCashOut, "Error", errorReporter, "In")

	// map ports
	wf.MapInPort("In", canCashOut, "In")
	wf.MapOutPort("Out", updateBalanceAfterCashOut, "Out")
	wf.MapOutPort("Error", errorReporter, "Out")

	return wf
}

package workflow

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/model"
	modelmaxine "gitlab.com/zenpagos/maxine/model"
	pbv1maxine "gitlab.com/zenpagos/maxine/proto/v1"
)

type UpdateBalanceAfterCashOut struct {
	goflow.Component

	In           <-chan *model.CashOut
	Out          chan<- *model.CashOut
	Error        chan<- error
	MaxineClient pbv1maxine.MaxineServiceClient
}

func (tsk *UpdateBalanceAfterCashOut) Process() {
	for co := range tsk.In {

		req := pbv1maxine.EntryRequest{
			Source:      modelmaxine.CashOutSource,
			AccountID:   co.AccountID.String(),
			SourceID:    co.ID.String(),
			Description: "this is a hardcoded description", // TODO: remove description constraint
			Amount:      co.Amount,
		}

		_, err := tsk.MaxineClient.CreateEntry(context.Background(), &req)
		if err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			return
		}

		tsk.Out <- co
	}
}

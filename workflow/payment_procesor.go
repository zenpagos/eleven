package workflow

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/model"
	pbv1lucas "gitlab.com/zenpagos/lucas/proto/v1"
	"gitlab.com/zenpagos/tools"
)

type PaymentProcessor struct {
	goflow.Component

	In                 <-chan *model.Payment
	Out                chan<- *model.Payment
	Error              chan<- error
	PaymentProcessFail chan<- *model.Payment
	LucasClient        pbv1lucas.LucasServiceClient
}

func (tsk *PaymentProcessor) Process() {
	for p := range tsk.In {

		req := pbv1lucas.PayOnlineWithDecidirRequest{
			// payment information
			PaymentMethod:       p.PaymentMethod,
			PaymentMethodType:   p.PaymentMethodType,
			Description:         p.Description,
			StatementDescriptor: tools.StringValueOrBlank(p.StatementDescriptor),
			Currency:            p.Currency,
			Amount:              p.PaidAmount,
			Installments:        p.Installments,
			CardBin:             tools.StringValueOrBlank(p.CardBin),

			// decidir information
			DecidirEnvironment:  "dev",
			DecidirPrivateKey:   tools.StringValueOrBlank(p.DecidirPrivateKey),
			DecidirPaymentToken: tools.StringValueOrBlank(p.DecidirPaymentToken),

			// merchant information
			MerchantTIN:            p.MerchantTIN,
			MerchantTINType:        p.MerchantTINType,
			MerchantName:           p.MerchantName,
			MerchantStreet:         p.MerchantStreet,
			MerchantDoorNumber:     p.MerchantDoorNumber,
			MerchantZipCode:        p.MerchantZipCode,
			MerchantCategory:       p.MerchantCategory,
			MerchantGeographicCode: p.MerchantGeographicCode,
			MerchantCity:           p.MerchantCity,
			MerchantID:             p.MerchantID,
			MerchantProvince:       p.MerchantProvince,
			MerchantCountry:        p.MerchantCountry,
			MerchantEmail:          p.MerchantEmail,
			MerchantPhone:          p.MerchantPhone,

			// payer information
			PayerTIN:   p.PayerTIN,
			PayerEmail: p.PayerEmail,
		}

		res, err := tsk.LucasClient.PayOnlineWithDecidir(context.Background(), &req)
		if err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			tsk.PaymentProcessFail <- p
			return
		}

		if res.Status == "approved" {
			p.Status = model.ApprovedStatus
			tsk.Out <- p
			return
		}

		p.Status = model.RejectedStatus

		tsk.Out <- p
	}
}

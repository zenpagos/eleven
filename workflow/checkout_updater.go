package workflow

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	pbv1dustin "gitlab.com/zenpagos/dustin/proto/v1"
	"gitlab.com/zenpagos/eleven/model"
)

type CheckoutUpdater struct {
	goflow.Component

	In           <-chan *model.Payment
	Out          chan<- *model.Payment
	Error        chan<- error
	DustinClient pbv1dustin.DustinServiceClient
}

func (tsk *CheckoutUpdater) Process() {
	for p := range tsk.In {

		req := pbv1dustin.CompleteCheckoutSessionRequest{
			ID: p.CheckoutSessionID.String(),
		}

		_, err := tsk.DustinClient.CompleteCheckoutSession(context.Background(), &req)
		if err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			return
		}

		tsk.Out <- p
	}
}

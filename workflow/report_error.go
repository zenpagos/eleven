package workflow

import (
	"github.com/trustmaster/goflow"
)

type ErrorReporter struct {
	goflow.Component

	In  <-chan error
	Out chan<- error
}

func (tsk *ErrorReporter) Process() {
	for e := range tsk.In {
		tsk.Out <- e
	}
}

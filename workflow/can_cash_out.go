package workflow

import (
	"context"
	"errors"
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/model"
	pbv1maxine "gitlab.com/zenpagos/maxine/proto/v1"
)

var errInsufficientBalance = errors.New("insufficient balance")

type CanCashOut struct {
	goflow.Component

	In           <-chan *model.CashOut
	Out          chan<- *model.CashOut
	Error        chan<- error
	MaxineClient pbv1maxine.MaxineServiceClient
}

func (tsk *CanCashOut) Process() {
	for co := range tsk.In {

		req := pbv1maxine.CanCashOutRequest{
			AccountID: co.AccountID.String(),
			Amount:    co.Amount,
		}

		res, err := tsk.MaxineClient.CanCashOut(context.Background(), &req)
		if err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			return
		}

		if !res.Result {
			log.WithError(errInsufficientBalance).Error()
			tsk.Error <- errInsufficientBalance
			return
		}

		tsk.Out <- co
	}
}

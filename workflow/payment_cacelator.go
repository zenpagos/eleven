package workflow

import (
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/eleven/repository"
)

type PaymentCancelator struct {
	goflow.Component

	In         <-chan *model.Payment
	Error      chan<- error
	Repository repository.PaymentRepositoryInterface
}

func (tsk *PaymentCancelator) Process() {
	for p := range tsk.In {
		p.Status = model.RejectedStatus

		if err := tsk.Repository.Update(p); err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			return
		}
	}
}

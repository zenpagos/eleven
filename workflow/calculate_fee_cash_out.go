package workflow

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/model"
	pbv1will "gitlab.com/zenpagos/will/proto/v1"
)

type CalculateFeeCashOut struct {
	goflow.Component

	In         <-chan *model.CashOut
	Out        chan<- *model.CashOut
	Error      chan<- error
	WillClient pbv1will.WillServiceClient
}

func (tsk *CalculateFeeCashOut) Process() {
	for co := range tsk.In {

		req := pbv1will.GetInformationToExecuteAPaymentRequest{
			Account:           co.AccountID.String(),
			PaymentMethodType: co.PaymentMethodType,
			PaymentMethod:     co.PaymentMethod,
			Installments:      co.Installments,
			Amount:            co.Amount,
		}

		res, err := tsk.WillClient.GetInformationToExecuteAPayment(context.Background(), &req)
		if err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			return
		}

		co.ProcessingCostPercentage = res.ProcessingCostPercentage
		co.ProcessingCostFixedAmount = res.ProcessingCostFixedAmount
		co.FeePercentage = res.FeePercentage
		co.FeeFixedAmount = res.FeeFixedAmount
		co.TaxesAmount = res.TaxesAmount
		co.FeeAmount = res.FeeAmount
		co.ReceivedAmount = res.ReceivedAmount

		tsk.Out <- co
	}
}

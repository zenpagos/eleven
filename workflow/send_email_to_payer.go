package workflow

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/config"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/eleven/wrapper/wkafka"
	"gitlab.com/zenpagos/robin/schema"
	"gitlab.com/zenpagos/tools"
)

type SendEmailToPayer struct {
	goflow.Component

	In     <-chan *model.Payment
	Out    chan<- *model.Payment
	Error  chan<- error
	WKafka *wkafka.KafkaWrapper
}

func (tsk *SendEmailToPayer) Process() {
	for p := range tsk.In {

		m := schema.PaymentApprovedSchema{
			PaymentMethod:     p.PaymentMethod,
			PaymentMethodType: p.PaymentMethodType,
			Description:       p.Description,
			PayerName:         tools.StringValueOrBlank(p.CardHolderName),
			PayerEmail:        p.PayerEmail,
			Currency:          p.Currency,
			PaidAmount:        p.PaidAmount,
			Installments:      p.Installments,
			InstallmentAmount: p.Installments,
		}
		msg, err := json.Marshal(m)
		if err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			return
		}

		if err := tsk.WKafka.PutMessage(config.PaymentApprovedTopic, msg); err != nil {
			log.WithError(err)
			tsk.Error <- err
			return
		}

		tsk.Out <- p
	}
}

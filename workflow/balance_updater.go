package workflow

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/model"
	modelmaxine "gitlab.com/zenpagos/maxine/model"
	pbv1maxine "gitlab.com/zenpagos/maxine/proto/v1"
)

type BalanceUpdater struct {
	goflow.Component

	In           <-chan *model.Payment
	Out          chan<- *model.Payment
	Error        chan<- error
	MaxineClient pbv1maxine.MaxineServiceClient
}

func (tsk *BalanceUpdater) Process() {
	for p := range tsk.In {

		req := pbv1maxine.EntryRequest{
			Source:      modelmaxine.PaymentSource,
			AccountID:   p.AccountID.String(),
			SourceID:    p.ID.String(),
			Description: p.Description,
			Amount:      p.Amount,
		}

		_, err := tsk.MaxineClient.CreateEntry(context.Background(), &req)
		if err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			return
		}

		tsk.Out <- p
	}
}

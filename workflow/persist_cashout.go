package workflow

import (
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/eleven/repository"
)

type PersistCashout struct {
	goflow.Component

	In         <-chan *model.CashOut
	Out        chan<- *model.CashOut
	Error      chan<- error
	Repository repository.CashOutRepositoryInterface
}

func (tsk *PersistCashout) Process() {
	for co := range tsk.In {

		if err := tsk.Repository.Insert(co); err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			return
		}

		tsk.Out <- co
	}
}

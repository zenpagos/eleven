package workflow

import (
	"context"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	pbv1dustin "gitlab.com/zenpagos/dustin/proto/v1"
	"gitlab.com/zenpagos/eleven/model"
)

type CheckoutGetter struct {
	goflow.Component

	In           <-chan *model.Payment
	Out          chan<- *model.Payment
	Error        chan<- error
	DustinClient pbv1dustin.DustinServiceClient
}

func (tsk *CheckoutGetter) Process() {
	for p := range tsk.In {

		req := pbv1dustin.GetPendingCheckoutSessionRequest{
			ID: p.CheckoutSessionID.String(),
		}

		res, err := tsk.DustinClient.GetPendingCheckoutSession(context.Background(), &req)
		if err != nil {
			log.WithError(err).Error()
			tsk.Error <- err
			return
		}

		cs := res.CheckoutSession
		p.AccountID = uuid.FromStringOrNil(cs.AccountID)
		p.Currency = cs.Currency
		p.Reference = cs.Reference
		p.Description = cs.Description
		p.Amount = cs.Total

		tsk.Out <- p
	}
}

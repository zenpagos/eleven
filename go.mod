module gitlab.com/zenpagos/eleven

go 1.14

require (
	github.com/asaskevich/govalidator v0.0.0-20200428143746-21a406dcc535 // indirect
	github.com/confluentinc/confluent-kafka-go v1.4.2
	github.com/denisenkom/go-mssqldb v0.0.0-20200428022330-06a60b6afbbc // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-ozzo/ozzo-validation/v4 v4.2.1
	github.com/go-testfixtures/testfixtures/v3 v3.2.0
	github.com/golang/protobuf v1.4.2
	github.com/google/go-cmp v0.4.1 // indirect
	github.com/google/wire v0.4.0
	github.com/gopherjs/gopherjs v0.0.0-20200217142428-fce0ec30dd00 // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/jinzhu/now v1.1.1 // indirect
	github.com/jnewmano/grpc-json-proxy v0.0.0-20200427184142-6696b5a3ab05
	github.com/kr/text v0.2.0 // indirect
	github.com/lib/pq v1.5.2 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/mitchellh/mapstructure v1.3.1 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.6.0
	github.com/smartystreets/assertions v1.1.0 // indirect
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.7.0
	github.com/trustmaster/goflow v0.0.0-20200418181934-7d201b964009
	gitlab.com/zenpagos/dustin v0.0.0-20200616235945-be81061286dd
	gitlab.com/zenpagos/lucas v0.0.0-20200616233439-4e7d2c50f96c
	gitlab.com/zenpagos/maxine v0.0.0-20200616231236-18596cd11ccb
	gitlab.com/zenpagos/robin v0.0.0-20200616233949-167822b2aed9
	gitlab.com/zenpagos/tools v0.0.0-20200616225453-f02e1e8b6545
	gitlab.com/zenpagos/will v0.0.0-20200617000436-69386059708f
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37 // indirect
	golang.org/x/net v0.0.0-20200520182314-0ba52f642ac2 // indirect
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
	google.golang.org/genproto v0.0.0-20200521103424-e9a78aa275b7 // indirect
	google.golang.org/grpc v1.29.1
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/ini.v1 v1.56.0 // indirect
)

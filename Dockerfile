#
# Use the Go image for building the service
#
FROM golang:1.14-alpine AS build

# Dependencies for confluent-kafka-go
RUN apk add --no-progress --no-cache gcc musl-dev

# Set the directory where we will copy and build
WORKDIR /go/src/gitlab.com/zenpagos/eleven

# Copy the files needed into the container
COPY . .

# Build the application
RUN GOOS=linux GOARCH=amd64 go build -tags musl -a -installsuffix cgo -o service cmd/main.go


#
# Use the alpine image for running the service
#
FROM alpine:latest

# Download dependencies and set the directory
RUN apk --no-cache add ca-certificates
WORKDIR /root/

# Copy the application binary from the build stage into the container
COPY --from=build /go/src/gitlab.com/zenpagos/eleven .

# Run the service application
CMD ["./service"]

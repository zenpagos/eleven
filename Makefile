#!make

wire:
	wire ./container

load_fixtures:
	go run scripts/fixtures/fixtures.go

run:
	docker-compose up --build $(arg)

deploy-local:
	docker build -t 127.0.0.1:5000/eleven .
	docker push 127.0.0.1:5000/eleven
	kubectl rollout restart -f kubernetes/eleven.yml

gen-proto:
	protoc -I proto/v1 proto/v1/eleven.proto --go_out=plugins=grpc:proto/v1

test:
	go test ./...

fmt:
	go fmt ./...
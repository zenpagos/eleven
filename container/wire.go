//+build wireinject

package container

import (
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	pbv1dustin "gitlab.com/zenpagos/dustin/proto/v1"
	"gitlab.com/zenpagos/eleven/config"
	"gitlab.com/zenpagos/eleven/repository/mysqlrepo"
	"gitlab.com/zenpagos/eleven/usecase/cashoutuc"
	"gitlab.com/zenpagos/eleven/usecase/paymentuc"
	"gitlab.com/zenpagos/eleven/wrapper/wkafka"
	pbv1lucas "gitlab.com/zenpagos/lucas/proto/v1"
	pbv1maxine "gitlab.com/zenpagos/maxine/proto/v1"
	pbv1will "gitlab.com/zenpagos/will/proto/v1"
)

func InitializeContainer(
	db *gorm.DB,
	kw *wkafka.KafkaWrapper,
	conf config.Configuration,
	mcl pbv1maxine.MaxineServiceClient,
	dcl pbv1dustin.DustinServiceClient,
	lcl pbv1lucas.LucasServiceClient,
	wcl pbv1will.WillServiceClient,
) Container {
	wire.Build(
		// repositories
		mysqlrepo.NewPaymentRepository,
		mysqlrepo.NewCashOutRepository,
		// use Cases
		paymentuc.NewPaymentUseCase,
		cashoutuc.NewCashOutUseCase,
		// container
		NewContainer,
	)

	return Container{}
}

package container

import "gitlab.com/zenpagos/eleven/usecase"

type Container struct {
	PaymentUseCase usecase.PaymentUseCaseInterface
	CashOutUseCase usecase.CashOutUseCaseInterface
}

func NewContainer(
	puc usecase.PaymentUseCaseInterface,
	couc usecase.CashOutUseCaseInterface,
) Container {
	return Container{
		PaymentUseCase: puc,
		CashOutUseCase: couc,
	}
}

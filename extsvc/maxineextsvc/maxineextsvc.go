package maxineextsvc

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	pbv1maxine "gitlab.com/zenpagos/maxine/proto/v1"
	"google.golang.org/grpc"
)

func NewGRPCClient(c MaxineConfiguration) (pbv1maxine.MaxineServiceClient, *grpc.ClientConn, error) {
	connectionUri := fmt.Sprintf("%s:%d", c.Host, c.Port)

	log.Tracef("connection to maxine: %s", connectionUri)

	conn, err := grpc.Dial(connectionUri, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil, nil, err
	}

	return pbv1maxine.NewMaxineServiceClient(conn), conn, nil
}

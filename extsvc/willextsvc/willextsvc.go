package willextsvc

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	pbv1will "gitlab.com/zenpagos/will/proto/v1"
	"google.golang.org/grpc"
)

func NewGRPCClient(c WillConfiguration) (pbv1will.WillServiceClient, *grpc.ClientConn, error) {
	connectionUri := fmt.Sprintf("%s:%d", c.Host, c.Port)

	log.Tracef("connection to will: %s", connectionUri)

	conn, err := grpc.Dial(connectionUri, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil, nil, err
	}

	return pbv1will.NewWillServiceClient(conn), conn, nil
}

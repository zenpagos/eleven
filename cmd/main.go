package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/eleven/config"
	"gitlab.com/zenpagos/eleven/container"
	"gitlab.com/zenpagos/eleven/extsvc/dustinextsvc"
	"gitlab.com/zenpagos/eleven/extsvc/lucasextsvc"
	"gitlab.com/zenpagos/eleven/extsvc/maxineextsvc"
	"gitlab.com/zenpagos/eleven/extsvc/willextsvc"
	"gitlab.com/zenpagos/eleven/interface/igrpc"
	"gitlab.com/zenpagos/eleven/proto/v1"
	"gitlab.com/zenpagos/eleven/repository/mysqlrepo"
	"gitlab.com/zenpagos/eleven/wrapper/wkafka"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net"
)

func main() {
	// read configuration
	conf, err := config.ReadConfig()
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	// loggin configurations
	log.SetReportCaller(true)
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(&log.JSONFormatter{})

	// connect to database
	db, err := mysqlrepo.NewMysqlClient(conf)
	if err != nil {
		log.Panicf("failed to connect to mysql: %v", err)
	}
	defer db.Close()

	// create kafka wrapper
	kw, err := wkafka.NewKafkaWrapper(conf)
	if err != nil {
		log.Panicf("error creating kafka wrapper: %v", err)
	}
	defer kw.Producer.Close()

	// connect to external services
	mcl, mclConn, err := maxineextsvc.NewGRPCClient(conf.Maxine)
	if err != nil {
		log.Panicf("failed to connect to lucas service: %v", err)
	}
	defer mclConn.Close()

	dcl, dclConn, err := dustinextsvc.NewGRPCClient(conf.Dustin)
	if err != nil {
		log.Panicf("failed to connect to dustin service: %v", err)
	}
	defer dclConn.Close()

	lcl, lclConn, err := lucasextsvc.NewGRPCClient(conf.Lucas)
	if err != nil {
		log.Panicf("failed to connect to lucas service: %v", err)
	}
	defer lclConn.Close()

	wcl, wclConn, err := willextsvc.NewGRPCClient(conf.Will)
	if err != nil {
		log.Panicf("failed to connect to will service: %v", err)
	}
	defer wclConn.Close()

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", conf.Service.Port))
	if err != nil {
		log.Panicf("failed to listen: %v", err)
	}

	c := container.InitializeContainer(db, kw, conf, mcl, dcl, lcl, wcl)

	s := grpc.NewServer()
	svc := igrpc.NewGRPCServer(c)
	pbv1.RegisterElevenServiceServer(s, svc)

	reflection.Register(s)

	if err := s.Serve(lis); err != nil {
		log.Panicf("failed to serve: %v", err)
	}
}

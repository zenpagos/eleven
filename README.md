# Eleven

## Requirements

* Create a mysql service
```shell script
docker network create --driver overlay --attachable cenpay

docker service create \
--env MYSQL_ROOT_PASSWORD=mysql \
--network cenpay \
--publish published=3306,target=3306 \
--mount type=volume,source=mysql,destination=/var/lib/mysql,volume-label="color=red",volume-label="shape=round" \
--name mysql mysql:8.0 
```
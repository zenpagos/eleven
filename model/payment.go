// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	uuid "github.com/satori/go.uuid"
)

type Payment struct {
	ID                uuid.UUID `sql:"type:varchar(255);primary_key;"`
	CreatedAt         int64     `sql:"type:int;not null"`
	UpdatedAt         *int64    `sql:"type:int"`
	Status            string    `sql:"type:varchar(25);not null"`
	CheckoutSessionID uuid.UUID `sql:"type:varchar(255);not null"`
	PaymentMethodType string    `sql:"type:varchar(255);not null"`
	PaymentMethod     string    `sql:"type:varchar(25)"`
	Installments      int64     `sql:"type:int;not null"`

	// these fields will be filled with information from checkout session
	AccountID   uuid.UUID `sql:"type:varchar(255);not null"`
	Currency    string    `sql:"type:varchar(3);not null"`
	Reference   string    `sql:"type:varchar(50);not null"`
	Description string    `sql:"type:varchar(255);not null"`
	Amount      int64     `sql:"type:int;not null"`

	// decidir information
	DecidirPrivateKey   *string `sql:"varchar(255)"`
	DecidirPaymentToken *string `sql:"varchar(255)"`

	// fee information
	CardCoefficient           float32 `sql:"type:decimal(14,4);not null"`
	ProcessingCostPercentage  float32 `sql:"type:decimal(5,2);not null"`
	ProcessingCostFixedAmount int64   `sql:"type:int;not null"`
	FeePercentage             float32 `sql:"type:decimal(5,2);not null"`
	FeeFixedAmount            int64   `sql:"type:int;not null"`
	DaysOnHold                int64   `sql:"type:int;not null"`
	InstallmentAmount         int64   `sql:"type:int;not null"`
	PaidAmount                int64   `sql:"type:int;not null"`
	TaxesAmount               int64   `sql:"type:int;not null"`
	FeeAmount                 int64   `sql:"type:int;not null"`
	ReceivedAmount            int64   `sql:"type:int;not null"`
	AmountReleaseDate         int64   `sql:"type:int;not null"`
	StatementDescriptor       *string `sql:"varchar(255)"`

	// merchant information
	MerchantTIN            string `sql:"varchar(255);not null"`
	MerchantTINType        string `sql:"varchar(255);not null"`
	MerchantName           string `sql:"varchar(255);not null"`
	MerchantStreet         string `sql:"varchar(255);not null"`
	MerchantDoorNumber     string `sql:"varchar(255);not null"`
	MerchantZipCode        string `sql:"varchar(255);not null"`
	MerchantCategory       string `sql:"varchar(255);not null"`
	MerchantGeographicCode string `sql:"varchar(255);not null"`
	MerchantCity           string `sql:"varchar(255);not null"`
	MerchantID             string `sql:"varchar(255);not null"`
	MerchantProvince       string `sql:"varchar(255);not null"`
	MerchantCountry        string `sql:"varchar(255);not null"`
	MerchantEmail          string `sql:"varchar(255);not null"`
	MerchantPhone          string `sql:"varchar(255);not null"`

	// payer information
	PayerTIN   string `sql:"type:varchar(8);"`
	PayerEmail string `sql:"type:varchar(255)"`

	// card information
	CardHolderName         *string `sql:"type:varchar(255)"`
	CardExpirationMonth    *int64  `sql:"type:int"`
	CardExpirationYear     *int64  `sql:"type:int"`
	CardNumberLength       *int64  `sql:"type:int"`
	CardSecurityCodeLength *int64  `sql:"type:int"`
	CardBin                *string `sql:"type:varchar(255)"`
	CardLastFourDigits     *string `sql:"type:varchar(255)"`
}

func (p Payment) Validate() error {
	isCC := p.PaymentMethodType == CreditCardPaymentMethodType
	isDB := p.PaymentMethodType == DebitCardPaymentMethodType
	isCard := isCC || isDB

	return validation.ValidateStruct(&p,
		validation.Field(&p.CheckoutSessionID, externalUUIDRule...),
		validation.Field(&p.PaymentMethodType, paymentMethodTypeRule...),
		validation.Field(&p.Installments, validation.Required, validation.Min(1), validation.Max(48)),

		// payer information
		validation.Field(&p.PayerTIN, is.Digit, validation.Required, validation.Length(7, 8)),
		validation.Field(&p.PayerEmail, is.Email, validation.Required, validation.Length(5, 255)),

		// card information
		validation.Field(
			&p.CardHolderName,
			validation.Required.When(isCard),
			validation.Length(0, 255),
		),
		validation.Field(
			&p.CardExpirationMonth,
			validation.Required.When(isCard),
		),
		validation.Field(
			&p.CardExpirationYear,
			validation.Required.When(isCard),
		),
		validation.Field(
			&p.CardNumberLength,
			validation.Required.When(isCard),
		),
		validation.Field(
			&p.CardSecurityCodeLength,
			validation.Required.When(isCard),
		),
		validation.Field(
			&p.CardBin,
			validation.Required.When(isCard),
		),
		validation.Field(
			&p.CardLastFourDigits,
			validation.Required.When(isCard),
		),
	)
}

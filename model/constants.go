// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

const (
	ACHPaymentMethodType        = "ach"
	CashPaymentMethodType       = "cash"
	CreditCardPaymentMethodType = "credit.card"
	DebitCardPaymentMethodType  = "debit.card"
	PEIPaymentMethodType        = "pei"
	VEPPaymentMethodType        = "vep"
	PlatformMethodType          = "platform"

	ARSCurrency = "ars"
	USDCurrency = "usd"

	PendingStatus  = "pending"
	ApprovedStatus = "approved"
	RejectedStatus = "rejected"
)

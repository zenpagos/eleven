// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	uuid "github.com/satori/go.uuid"
)

type CashOut struct {
	ID                uuid.UUID `sql:"type:varchar(255);primary_key;"`
	CreatedAt         int64     `sql:"type:int;not null"`
	UpdatedAt         *int64    `sql:"type:int"`
	Status            string    `sql:"type:varchar(25);not null"`
	BankAccountID     uuid.UUID `sql:"type:varchar(255);not null"`
	PaymentMethodType string    `sql:"type:varchar(255);not null"`
	PaymentMethod     string    `sql:"type:varchar(25)"`
	Installments      int64     `sql:"type:int;not null"`
	AccountID         uuid.UUID `sql:"type:varchar(255);not null"`
	Currency          string    `sql:"type:varchar(3);not null"`
	Amount            int64     `sql:"type:int;not null"`

	// this information is calculated by will service
	ProcessingCostPercentage  float32 `sql:"type:decimal(5,2);not null"`
	ProcessingCostFixedAmount int64   `sql:"type:int;not null"`
	FeePercentage             float32 `sql:"type:decimal(5,2);not null"`
	FeeFixedAmount            int64   `sql:"type:int;not null"`
	TaxesAmount               int64   `sql:"type:int;not null"`
	FeeAmount                 int64   `sql:"type:int;not null"`
	ReceivedAmount            int64   `sql:"type:int;not null"`
}

func (co CashOut) Validate() error {
	return validation.ValidateStruct(&co,
		validation.Field(&co.AccountID, externalUUIDRule...),
		validation.Field(&co.BankAccountID, externalUUIDRule...),
		validation.Field(&co.Amount, validation.Required),
	)
}

package wkafka

import (
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/eleven/config"
)

type KafkaWrapper struct {
	Producer *kafka.Producer
}

func NewKafkaWrapper(conf config.Configuration) (*KafkaWrapper, error) {
	bkrAddr := fmt.Sprintf("%s:%d", conf.Kafka.Host, conf.Kafka.Port)
	kp, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": bkrAddr})
	if err != nil {
		log.WithError(err)
		return nil, err
	}

	kw := &KafkaWrapper{
		Producer: kp,
	}

	return kw, nil
}

func (kw KafkaWrapper) PutMessage(topic string, msg []byte) error {

	km := &kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic:     &topic,
			Partition: kafka.PartitionAny,
		},
		Value: msg,
	}

	if err := kw.Producer.Produce(km, nil); err != nil {
		log.WithError(err)
		return err
	}

	return nil
}

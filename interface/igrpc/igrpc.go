package igrpc

import (
	"context"
	_ "github.com/jnewmano/grpc-json-proxy/codec"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/eleven/container"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/eleven/proto/v1"
	"gitlab.com/zenpagos/tools"
)

type gRPCServer struct {
	container container.Container
}

func NewGRPCServer(c container.Container) pbv1.ElevenServiceServer {
	return &gRPCServer{
		container: c,
	}
}

func (s *gRPCServer) CreatePayment(
	_ context.Context,
	req *pbv1.CreatePaymentRequest,
) (*pbv1.CreatePaymentResponse, error) {

	p := &model.Payment{
		CheckoutSessionID:      uuid.FromStringOrNil(req.CheckoutSessionID),
		DecidirPaymentToken:    tools.StringPointerOrNil(req.PaymentToken),
		PaymentMethodType:      req.PaymentMethodType,
		PaymentMethod:          req.PaymentMethod,
		Installments:           req.Installments,
		PayerTIN:               req.PayerTIN,
		PayerEmail:             req.PayerEmail,
		CardHolderName:         tools.StringPointerOrNil(req.CardHolderName),
		CardExpirationMonth:    tools.Int64PointerOrNil(req.CardExpirationMonth),
		CardExpirationYear:     tools.Int64PointerOrNil(req.CardExpirationYear),
		CardNumberLength:       tools.Int64PointerOrNil(req.CardNumberLength),
		CardSecurityCodeLength: tools.Int64PointerOrNil(req.CardSecurityCodeLength),
		CardBin:                tools.StringPointerOrNil(req.CardBin),
		CardLastFourDigits:     tools.StringPointerOrNil(req.CardLastFourDigits),
	}

	if err := s.container.PaymentUseCase.CreatePayment(p); err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	pr := &pbv1.Payment{
		ID:                     p.ID.String(),
		CreatedAt:              p.CreatedAt,
		UpdatedAt:              tools.Int64ValueOrZero(p.UpdatedAt),
		Status:                 p.Status,
		CheckoutSessionID:      p.CheckoutSessionID.String(),
		PaymentMethodType:      p.PaymentMethod,
		Installments:           p.Installments,
		PayerTIN:               p.PayerTIN,
		PayerEmail:             p.PayerEmail,
		CardHolderName:         tools.StringValueOrBlank(p.CardHolderName),
		CardExpirationYear:     tools.Int64ValueOrZero(p.CardExpirationYear),
		CardExpirationMonth:    tools.Int64ValueOrZero(p.CardExpirationMonth),
		CardNumberLength:       tools.Int64ValueOrZero(p.CardNumberLength),
		CardSecurityCodeLength: tools.Int64ValueOrZero(p.CardSecurityCodeLength),
		CardBin:                tools.StringValueOrBlank(p.CardBin),
		CardLastFourDigits:     tools.StringValueOrBlank(p.CardLastFourDigits),
	}

	res := &pbv1.CreatePaymentResponse{
		Payment: pr,
	}

	return res, nil
}

func (s *gRPCServer) ListPayments(
	_ context.Context,
	req *pbv1.ListPaymentsRequest,
) (*pbv1.ListPaymentsResponse, error) {

	aid := req.AccountID

	payments, err := s.container.PaymentUseCase.ListPayments(aid)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	var ps []*pbv1.Payment
	for _, p := range payments {
		ps = append(ps, &pbv1.Payment{
			ID:                     p.ID.String(),
			CreatedAt:              p.CreatedAt,
			UpdatedAt:              tools.Int64ValueOrZero(p.UpdatedAt),
			Status:                 p.Status,
			CheckoutSessionID:      p.CheckoutSessionID.String(),
			PaymentMethodType:      p.PaymentMethod,
			Installments:           p.Installments,
			PayerTIN:               p.PayerTIN,
			PayerEmail:             p.PayerEmail,
			CardHolderName:         tools.StringValueOrBlank(p.CardHolderName),
			CardExpirationYear:     tools.Int64ValueOrZero(p.CardExpirationYear),
			CardExpirationMonth:    tools.Int64ValueOrZero(p.CardExpirationMonth),
			CardNumberLength:       tools.Int64ValueOrZero(p.CardNumberLength),
			CardSecurityCodeLength: tools.Int64ValueOrZero(p.CardSecurityCodeLength),
			CardBin:                tools.StringValueOrBlank(p.CardBin),
			CardLastFourDigits:     tools.StringValueOrBlank(p.CardLastFourDigits),
		})
	}

	res := &pbv1.ListPaymentsResponse{
		Payments: ps,
	}

	return res, nil
}

func (s *gRPCServer) CreateCashOut(
	_ context.Context,
	req *pbv1.CreateCashOutRequest,
) (*pbv1.CreateCashOutResponse, error) {

	co := &model.CashOut{
		AccountID:     uuid.FromStringOrNil(req.AccountID),
		BankAccountID: uuid.FromStringOrNil(req.BankAccountID),
		Amount:        req.Amount,
	}

	if err := s.container.CashOutUseCase.CreateCashOut(co); err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	res := &pbv1.CreateCashOutResponse{
		ID:            co.ID.String(),
		CreatedAt:     co.CreatedAt,
		Status:        co.Status,
		AccountID:     co.AccountID.String(),
		BankAccountID: co.BankAccountID.String(),
		Amount:        co.Amount,
	}

	return res, nil
}

func (s *gRPCServer) ListCashOuts(
	_ context.Context,
	req *pbv1.ListCashOutsRequest,
) (*pbv1.ListCashOutsResponse, error) {

	aid := req.AccountID

	payments, err := s.container.CashOutUseCase.ListCashOuts(aid)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	var cos []*pbv1.CashOut
	for _, co := range payments {
		cos = append(cos, &pbv1.CashOut{
			ID:            co.ID.String(),
			CreatedAt:     co.CreatedAt,
			UpdatedAt:     tools.Int64ValueOrZero(co.UpdatedAt),
			Status:        co.Status,
			AccountID:     co.AccountID.String(),
			BankAccountID: co.BankAccountID.String(),
			Amount:        co.Amount,
		})
	}

	res := &pbv1.ListCashOutsResponse{
		CashOuts: cos,
	}

	return res, nil
}

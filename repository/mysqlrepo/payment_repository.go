package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/eleven/repository"
)

type PaymentRepository struct {
	DB *gorm.DB
}

func NewPaymentRepository(db *gorm.DB) repository.PaymentRepositoryInterface {
	return PaymentRepository{
		DB: db,
	}
}

func (pr PaymentRepository) Insert(cs *model.Payment) error {
	return pr.DB.Create(&cs).Error
}

func (pr PaymentRepository) Update(cs *model.Payment) error {
	return pr.DB.Save(&cs).Error
}

func (pr PaymentRepository) FindByAccountID(ai string) ([]model.Payment, error) {
	var ps []model.Payment
	if err := pr.DB.Where("account_id = ?", ai).Find(&ps).Error; err != nil {
		return nil, err
	}

	return ps, nil
}

package mysqlrepo

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/eleven/config"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/tools"
)

const DatabaseDialect = "mysql"

func NewMysqlClient(c config.Configuration) (*gorm.DB, error) {
	connectionUri := fmt.Sprintf(
		"%s:%s@(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		c.Database.Username,
		c.Database.Password,
		c.Database.Host,
		c.Database.Database,
	)

	if c.Service.Prod == false {
		log.Tracef("connecting to: %s", connectionUri)
	}

	db, err := gorm.Open(DatabaseDialect, connectionUri)
	if err != nil {
		return nil, err
	}

	db.Callback().
		Create().
		Before("gorm:create").
		After("gorm:before_create").
		Register("generateUUID:before_create", generateUUID)

	db.Callback().
		Create().
		Replace("gorm:update_time_stamp", updateTimeStampForCreateCallback)

	db.Callback().
		Update().
		Replace("gorm:update_time_stamp", updateTimeStampForUpdateCallback)

	db.AutoMigrate(
		&model.Payment{},
		&model.CashOut{},
		&model.CashOut{},
	)

	if c.Service.Prod == false {
		db.LogMode(true)
	}

	return db, nil
}

func generateUUID(scope *gorm.Scope) {
	if !scope.HasError() {
		id := uuid.NewV4()

		if IDField, ok := scope.FieldByName("ID"); ok {
			if IDField.IsBlank {
				IDField.Set(id)
			}
		}
	}
}

func updateTimeStampForCreateCallback(scope *gorm.Scope) {
	if !scope.HasError() {
		now := tools.NewUnixTime()

		if createdAtField, ok := scope.FieldByName("CreatedAt"); ok {
			if createdAtField.IsBlank {
				createdAtField.Set(now)
			}
		}
	}
}

func updateTimeStampForUpdateCallback(scope *gorm.Scope) {
	if _, ok := scope.Get("gorm:update_column"); !ok {
		now := tools.NewUnixTime()
		scope.SetColumn("UpdatedAt", now)
	}
}

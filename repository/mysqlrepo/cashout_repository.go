package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/eleven/repository"
)

type CashOutRepository struct {
	DB *gorm.DB
}

func NewCashOutRepository(db *gorm.DB) repository.CashOutRepositoryInterface {
	return CashOutRepository{
		DB: db,
	}
}

func (cor CashOutRepository) Insert(c *model.CashOut) error {
	return cor.DB.Create(&c).Error
}

func (cor CashOutRepository) FindByAccountID(ai string) ([]model.CashOut, error) {
	var co []model.CashOut
	if err := cor.DB.Where("account_id = ?", ai).Find(&co).Error; err != nil {
		return nil, err
	}

	return co, nil
}

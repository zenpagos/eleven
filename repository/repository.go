// Package repository and it's sub-package represents data persistence service, mainly access database,
// but also including data persisted by other Micro-service.
// For Micro-server, only the interface is defined in this package, the data transformation code is in adapter package
// This is the top level package and it only defines interface, and all implementations are defined in sub-package
// Use case package depends on it.
package repository

import "gitlab.com/zenpagos/eleven/model"

type PaymentRepositoryInterface interface {
	Insert(p *model.Payment) error
	Update(p *model.Payment) error
	FindByAccountID(ai string) ([]model.Payment, error)
}

type CashOutRepositoryInterface interface {
	Insert(c *model.CashOut) error
	FindByAccountID(ai string) ([]model.CashOut, error)
}

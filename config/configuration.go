package config

import (
	"gitlab.com/zenpagos/eleven/extsvc/dustinextsvc"
	"gitlab.com/zenpagos/eleven/extsvc/lucasextsvc"
	"gitlab.com/zenpagos/eleven/extsvc/maxineextsvc"
	"gitlab.com/zenpagos/eleven/extsvc/willextsvc"
)

type Configuration struct {
	Service  ServiceConfiguration
	Database DatabaseConfiguration
	Kafka    KafkaConfiguration
	Maxine   maxineextsvc.MaxineConfiguration
	Dustin   dustinextsvc.DustinConfiguration
	Lucas    lucasextsvc.LucasConfiguration
	Will     willextsvc.WillConfiguration
}

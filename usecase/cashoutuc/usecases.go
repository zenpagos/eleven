// Package registration represents the concrete implementation of CashOutUseCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package cashoutuc

import (
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/eleven/repository"
	"gitlab.com/zenpagos/eleven/usecase"
	"gitlab.com/zenpagos/eleven/workflow"
	pbv1maxine "gitlab.com/zenpagos/maxine/proto/v1"
	pbv1will "gitlab.com/zenpagos/will/proto/v1"
)

type CashOutUseCase struct {
	CashOutRepository repository.CashOutRepositoryInterface
	MaxineClient      pbv1maxine.MaxineServiceClient
	WillClient        pbv1will.WillServiceClient
}

func NewCashOutUseCase(
	cr repository.CashOutRepositoryInterface,
	mcl pbv1maxine.MaxineServiceClient,
	wcl pbv1will.WillServiceClient,
) usecase.CashOutUseCaseInterface {
	return CashOutUseCase{
		CashOutRepository: cr,
		MaxineClient:      mcl,
		WillClient:        wcl,
	}
}

func (couc CashOutUseCase) CreateCashOut(co *model.CashOut) error {
	co.Status = model.ApprovedStatus
	co.Installments = 1
	co.Currency = model.ARSCurrency
	co.PaymentMethod = model.PlatformMethodType
	co.PaymentMethodType = model.PlatformMethodType

	// validate the information received for the user
	if err := co.Validate(); err != nil {
		log.Errorf("validation errors %v", err)
		return err
	}

	wf := workflow.NewCashOutWorkflow(
		couc.CashOutRepository,
		couc.MaxineClient,
		couc.WillClient,
	)

	in := make(chan *model.CashOut)
	out := make(chan *model.CashOut)
	err := make(chan error)

	wf.SetInPort("In", in)
	wf.SetOutPort("Out", out)
	wf.SetOutPort("Error", err)

	wait := goflow.Run(wf)

	in <- co

	close(in)

	<-out

	if e := <-err; e != nil {
		return e
	}

	<-wait

	return nil
}

func (couc CashOutUseCase) ListCashOuts(aid string) ([]model.CashOut, error) {
	return couc.CashOutRepository.FindByAccountID(aid)
}

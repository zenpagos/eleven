// Package registration represents the concrete implementation of PaymentUseCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package paymentuc

import (
	log "github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	pbv1dustin "gitlab.com/zenpagos/dustin/proto/v1"
	"gitlab.com/zenpagos/eleven/model"
	"gitlab.com/zenpagos/eleven/repository"
	"gitlab.com/zenpagos/eleven/usecase"
	"gitlab.com/zenpagos/eleven/workflow"
	"gitlab.com/zenpagos/eleven/wrapper/wkafka"
	pbv1lucas "gitlab.com/zenpagos/lucas/proto/v1"
	pbv1maxine "gitlab.com/zenpagos/maxine/proto/v1"
	pbv1will "gitlab.com/zenpagos/will/proto/v1"
)

type PaymentUseCase struct {
	PaymentRepository repository.PaymentRepositoryInterface
	WKafka            *wkafka.KafkaWrapper
	MaxineClient      pbv1maxine.MaxineServiceClient
	LucasClient       pbv1lucas.LucasServiceClient
	DustinClient      pbv1dustin.DustinServiceClient
	WillClient        pbv1will.WillServiceClient
}

func NewPaymentUseCase(
	pr repository.PaymentRepositoryInterface,
	kw *wkafka.KafkaWrapper,
	mcl pbv1maxine.MaxineServiceClient,
	lcl pbv1lucas.LucasServiceClient,
	dcl pbv1dustin.DustinServiceClient,
	wcl pbv1will.WillServiceClient,
) usecase.PaymentUseCaseInterface {
	return PaymentUseCase{
		PaymentRepository: pr,
		WKafka:            kw,
		MaxineClient:      mcl,
		LucasClient:       lcl,
		DustinClient:      dcl,
		WillClient:        wcl,
	}
}

func (puc PaymentUseCase) CreatePayment(p *model.Payment) error {
	// create payment with pending status
	p.Status = model.PendingStatus

	// validate the information received for the user
	if err := p.Validate(); err != nil {
		log.WithError(err).Error()
		return err
	}

	wf := workflow.NewPaymentWithCardWorkflow(
		puc.PaymentRepository,
		puc.WKafka,
		puc.DustinClient,
		puc.LucasClient,
		puc.MaxineClient,
		puc.WillClient,
	)

	in := make(chan *model.Payment)
	out := make(chan *model.Payment)
	err := make(chan error)

	wf.SetInPort("In", in)
	wf.SetOutPort("Out", out)
	wf.SetOutPort("Error", err)

	wait := goflow.Run(wf)

	in <- p

	close(in)

	<-out

	if e := <-err; e != nil {
		return e
	}

	<-wait

	return nil
}

func (puc PaymentUseCase) ListPayments(aid string) ([]model.Payment, error) {
	return puc.PaymentRepository.FindByAccountID(aid)
}
